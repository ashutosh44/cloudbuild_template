## Create build 


1. Create your deployment as described below, replacing `<YOUR_DEPLOYMENT_NAME>`
   with your with your own deployment name

    ```shell
    gcloud deployment-manager deployments create <YOUR_DEPLOYMENT_NAME>  --config my_cloudbuild.yaml
    ```

2. In case you need to delete your deployment:

    ```shell
    gcloud deployment-manager deployments delete <YOUR_DEPLOYMENT_NAME>
    ```
